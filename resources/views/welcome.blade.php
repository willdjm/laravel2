@extends('layouts.main')

@section('title', 'IMOB WILL')

@section('content')

<div id="search-container" class="col-md-12">
    <h1>Busque um Imóvel</h1>
    <form action="">
    <input type="text" id="search" name="search" class="form-control" placeholder="Procurar....">
    </form>
</div>

<div id="imoveis-container" class="col-md-12">
    <h2>Imóveis em Destaque</h2>
    <div id="cards-container" class="row">
        @foreach($events as $event)
        <div class="card col-md-3">
            <img src="/img/event_placeholder.jpg" alt="{{$event->title}}">
            <div class="card-body">
                <p class="card-date"></p>
                <h5 class="card-title">{{ $event->tile }}</h5>
                <p class="card-participantes">X PARTS</p>
                <a href="#" class="btn btn-primary">Ver Ficha Completa</a>
            </div>
        </div>
        
        @endforeach
    </div>
</div>

@endsection