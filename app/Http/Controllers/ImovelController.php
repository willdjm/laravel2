<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;


class ImovelController extends Controller
{
    public function index(){
        
        $events = Event::all();
    
        return view('welcome', ['events' => $events]);
    }

    public function cadastrar(){
        return view('imoveis.cadastrar');
    }
}
